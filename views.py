from django.shortcuts import render,redirect
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView,CreateAPIView
from .serializers import *
from .models import *
import json
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,viewsets
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from .tokens import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth import  login
from django.core.mail import BadHeaderError, send_mail
from rest_framework.permissions import IsAuthenticated
from .forms import *
from incidents.models import IncidentReport,ReporterDetails
from incidents.serializers import IncidentReportSerializer
from incidents.permission import IsAuthenticatedEx,IsAuthenticatedIn
from django.db import connection
#from django.http import HttpResponse


# Create your views here.

def getuserrolesdetails1(request,roleid):
    roledata={
        'status':'false'
    }
    cursor = connection.cursor()
    #with connection.cursor() as cursor:
    cursor.execute("""
    SELECT aur.roles_id, ar.role_name FROM `accounts_userrole_roles` AS aur LEFT join accounts_userrole AS au ON au.id=aur.userrole_id 
    LEFT JOIN accounts_roles AS ar ON ar.role_id=aur.roles_id WHERE au.userid_id='%s' GROUP BY aur.roles_id, ar.role_name
    """%(roleid))
    rolevalue=cursor.fetchall()
    print(rolevalue)
    if rolevalue:
        roledata = {
            'status': 'true'
        }
        dictvalue = []
        for i in rolevalue:
            temp = {}
            id=i[0]
            temp['id'] = i[0]
            temp['role'] = i[1]
            dictvalue.append(temp)
        roledata['roles'] = dictvalue
    return JsonResponse(roledata)
    #return JsonResponse(roledata)


class getuserrolesdetails(ListCreateAPIView):
    """
     This function is used to get info about roels assigned to user
     """

    def get(self,request,roleid):
        # queryset = UserRole.objects.filter(userid=roleid)
        # data = UserRoleSerializer3(queryset, many=True).data
        queryset = UserRole.objects.filter(userid=roleid)
        data = UserRoleSerializer1(queryset, many=True).data

        return JsonResponse({"status": True, "data": data})

        #return JsonResponse({"status": True, "data": data})
class UsersFormDetails(RetrieveUpdateDestroyAPIView):
    """
    This function is used to get specific user data
    """
    def get(self, request,pk):
        upobj = User.objects.get(pk=pk)
        ans = UsersSerializer(upobj)
        return Response(ans.data)


class UserFormDetails(RetrieveUpdateDestroyAPIView):
    """
        This function is used to Delete and update user data
        """
    permission_classes = (IsAuthenticated,IsAuthenticatedIn,)
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def delete(self, request,pk):
        #print('i am here')
        upobj = UserProfile.objects.get(pk=pk)
        user = User.objects.get(pk=upobj.user.id)
        user.delete()
        return JsonResponse({"status": True})

    def get(self, request,pk):
        upobj = UserProfile.objects.get(pk=pk)
        ans = UserProfiledetailSerializer(upobj)
        return Response(ans.data)

    def put(self, request,pk):
        data=request.data
        upobj = UserProfile.objects.get(pk=pk)
        useronj = User.objects.get(pk=upobj.user.id)
        #print(useronj)
        upobj.user_manager.clear()
        upobj.user_category.clear()
        upobj.user_role_id.clear()
        userpcat = []
        userroleids = []
        catdata = data.pop('usercat')
        for key, value in catdata.items():
            a = value
            # for key, value in a.items():
            #  print(key,value)
            # print( a['id'])
            if a['id'] != 0:
                userroleid = UserRole.objects.get(pk=a['id'])
                userroleid.roles.clear()
            if a['id'] == 0:
                  userroleid = UserRole.objects.create(userid=useronj)
            userroleids.append(userroleid.pk)
            userpcat.append(a['user_category'][0])
            categorinfo = IncidentCategory.objects.get(cat_id=a['user_category'][0])
            userroleid.catagory = categorinfo
            for e in a['user_role']:
                rinfo = Roles.objects.get(role_id=e)
                userroleid.roles.add(rinfo)
                userroleid.save()
        data['user_role_id'] = userroleids
        data['user_category'] = userpcat
        upobj.user_firstname = data['user_firstname']
        upobj.user_lastname = data['user_lastname']
        upobj.user_grp_emailID = data['user_grp_emailID']
        upobj.user_phone = data['user_phone']
        upobj.user_mobile = data['user_mobile']
        for um in data['user_manager']:
            upobj.user_manager.add(um)
        for ur in data['user_role_id']:
            upobj.user_role_id.add(ur)
        for uc in data['user_category']:
            upobj.user_category.add(uc)
        upobj.save()
        return JsonResponse({"status": True})


class orginfo(RetrieveUpdateDestroyAPIView):
    """
            This function is used  and update organisation data
            """
    permission_classes = (IsAuthenticated,IsAuthenticatedIn,)
    queryset = Organisation.objects.all()
    serializer_class = OrganisationFormSerializer

    def put(self, request,pk):
        data=request.data
        orgobj = Organisation.objects.get(pk=pk)
        orgobj.org_cat_tools.clear()
        orgobj.org_roles.clear()
        orgobj.org_name = data['org_name']
        orgobj.org_address = data['org_address']
        orgobj.org_email = data['org_email']
        orgobj.org_timezone = data['org_timezone']
        orgobj.org_date_format = data['org_date_format']
        orgobj.org_time_format = data['org_time_format']
        org_roles = data.pop('org_roles')
        for orgt in org_roles:
            orgobj.org_roles.add(orgt)
        catdata = data.pop('orgcat')
        orgcatids = []
        for key, value in catdata.items():
            a = value
            if a['id'] != 0:
                orgcatid = OrgCatTools.objects.get(pk=a['id'])
                orgcatid.org_tools.clear()
            if a['id'] == 0:
                orgcatid = OrgCatTools.objects.create(org=orgobj)
            orgcatids.append(orgcatid.pk)
            # for key, value in a.items():
            # userpcat.append(a['org_category'][0])
            categorinfo = IncidentCategory.objects.get(cat_id=a['org_category'][0])
            orgcatid.org_incident_categories = categorinfo
            for e in a['org_tool']:
                tinfo = Tools.objects.get(tool_id=e)
                orgcatid.org_tools.add(tinfo)
            orgcatid.save()
        for orgca in orgcatids:
            orgobj.org_cat_tools.add(orgca)
        orgobj.save()
        return JsonResponse({"status": True})



class OrganisationFormView(APIView):
    """
                This function is used to create organisation and list those
                """
    permission_classes = (IsAuthenticated,IsAuthenticatedIn,)
    queryset = Organisation.objects.all()
    serializer_class = OrganisationFormSerializer

    def post(self, request, format=None):
        data = request.data
        catdata = data.pop('orgcat')
        org_roles = data.pop('org_roles')
        #print(data)
        orgdata = Organisation.objects.create(**data)
        orgcatids = []
        for key, value in catdata.items():
            a = value
            orgcatid = OrgCatTools.objects.create(org=orgdata)
            orgcatids.append(orgcatid.pk)
            # for key, value in a.items():
            #userpcat.append(a['org_category'][0])
            categorinfo = IncidentCategory.objects.get(cat_id=a['org_category'][0])
            orgcatid.org_incident_categories = categorinfo
            for e in a['org_tool']:
                tinfo = Tools.objects.get(tool_id=e)
                orgcatid.org_tools.add(tinfo)
            orgcatid.save()
        # for orgt in org_tools:
        #     orgdata.org_tools.add(orgt)
        # for orgc in org_incident_categories:
        #         orgdata.org_incident_categories.add(orgc)
        for orgr in org_roles:
            orgdata.org_roles.add(orgr)
        for orgcati in orgcatids:
            orgdata.org_cat_tools.add(orgcati)
        orgdata.save()
        # serializer = OrganisationFormSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        return JsonResponse({"status": True,"orgid":orgdata.org_id})

    def get(self,request):
        queryset = Organisation.objects.all()
        data = OrganisationFormSerializer(queryset, many=True).data

        return JsonResponse({"status": True, "data": data})


class userroleinfo(ListCreateAPIView):
    """
                This function is used to get info about roels assigned to user
                """
    permission_classes = (IsAuthenticated,)
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def get(self,request,uid):
        queryset = UserRole.objects.filter(userid=uid)
        data = UserRoleSerializer1(queryset, many=True).data

        return JsonResponse({"status": True, "data": data})


class UserFormView(ListCreateAPIView):
    """
                    This function is used to create and sent activation link for user
    """
    permission_classes = (IsAuthenticated,IsAuthenticatedIn,)
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def post(self, request):
        data = request.data
        user_data = data.pop('user')
        userdata = User.objects.create(**user_data)
        data['user'] = userdata.id
        userroleids=[]
        userpcat=[]
        # userroleid = UserRole.objects.create(userid=userdata)
        catdata = data.pop('usercat')
        for key, value in catdata.items():
            a=value
            userroleid = UserRole.objects.create(userid=userdata)
            userroleids.append(userroleid.pk)
            for key,value in a.items():
                userpcat.append(a['user_category'][0])
                categorinfo = IncidentCategory.objects.get(cat_id=a['user_category'][0])
                userroleid.catagory = categorinfo
                for e in a['user_role']:
                    rinfo = Roles.objects.get(role_id=e)
                    userroleid.roles.add(rinfo)
                    userroleid.save()
        # for e in data['user_category']:
        #     categorinfo = IncidentCategory.objects.get(cat_id=e)
        # userroleid = UserRole.objects.create(userid=userdata, catagory=categorinfo)
        # for ea in data['user_role']:
        #     rinfo = Roles.objects.get(role_id=ea)
        #     userroleid.roles.add(rinfo)
        #     userroleid.save()
        #print(userroleids)
        data['user_role_id']=userroleids
        data['user_category'] = userpcat
        userprofileobj = UserProfile.objects.get(user=userdata)
        userprofileobj.user_firstname = data['user_firstname']
        userprofileobj.user_lastname = data['user_lastname']
        userprofileobj.user_grp_emailID = data['user_grp_emailID']
        for um in data['user_manager']:
            userprofileobj.user_manager.add(um)
        for ur in data['user_role_id']:
            userprofileobj.user_role_id.add(ur)
        for uc in data['user_category']:
            userprofileobj.user_category.add(uc)
        userprofileobj.user_phone = data['user_phone']
        userprofileobj.user_mobile = data['user_mobile']
        userprofileobj.save()
        current_site = get_current_site(request)
        subject = 'Activate Your  Account'
        message = render_to_string('email.html', {
            'user': userdata,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(userdata.pk)),
            'token': account_activation_token.make_token(userdata),
        })
        #userdata.email(subject, message)
        email_from = 'nevillegonsalves80@gamil.com'
        send_mail(subject, message,email_from, [userdata.email,])
        return JsonResponse({"status": True,"userid":userprofileobj.id})





# class UserFormDetail(RetrieveUpdateDestroyAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


# def OrganisationDetail(request):
#     org_detail = Organisation.objects.all()
#     return render(request, 'org_details.html', {'org_detail': org_detail})



class RolesView(ListCreateAPIView):
    """
                    This function is used to view all roles
                    """
    def get(self, request, **kwargs):
        queryset = Roles.objects.all()
        data = RolesSerializer(queryset,many=True).data

        return JsonResponse({"status": True, "data": data})


class RolesDetail(RetrieveUpdateDestroyAPIView):
    """
                        This function is used to delete and update roles
    """
    queryset = Roles.objects.all()
    serializer_class = RolesSerializer


class IncidentCategoryView(ListCreateAPIView):
    """
                        This function is used to view all incident categories
    """

    def get(self, request, **kwargs):
        queryset = IncidentCategory.objects.all()
        data = IncidentCategorySerializer(queryset,many=True).data

        return JsonResponse({"status": True, "data": data})



class IncidentCategoryDetail(RetrieveUpdateDestroyAPIView):
    """
     This function is used to update and delete category
    """
    queryset = IncidentCategory.objects.all()
    serializer_class = IncidentCategorySerializer


class SubCategoryView(ListCreateAPIView):
    """
         This function is used to list and create sub category
        """
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer


class SubCategoryDetail(RetrieveUpdateDestroyAPIView):
    """
             This function is used to delete sub category
            """
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer


class ToolsView(ListCreateAPIView):

    def get(self, request, **kwargs):
        queryset = Tools.objects.all()
        data = ToolsSerializer(queryset,many=True).data

        return JsonResponse({"status": True, "data": data})


class ToolsDetail(RetrieveUpdateDestroyAPIView):
    queryset = Tools.objects.all()
    serializer_class = ToolsSerializer


class ManagerView(ListCreateAPIView):

    def get(self, request, **kwargs):

        queryset = User.objects.filter(groups__name='Manager')
        #data = UserSerializer(queryset,many=True).data
        data = '11'
        return JsonResponse({"status": True, "data": data})


class UsersView(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UsersSerializer


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user1 = UserProfile.objects.get(user=uid)
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user1 = None
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        # user.is_active = True
        user1.is_verified = True
        user1.save()
        login(request, user)
        return redirect("http://13.233.70.203:3000/resetPassword?resetid="+(uidb64))
    else:
        return render(request, 'nothanks.html')


# def editprofile(request,uid):
#     #print(uid)
#     id = uid
#     user1 = UserProfile.objects.all()
#     # useroleobj = UserRole.objects.filter()
#     # if request.method == 'POST':
#     #     print(request.POST)
#     #     form = ProfileUpdateForm(request.POST, instance=user1)
#     #     user_role_form = []
#     #     for obj in useroleobj:
#     #         user_role_form.append(UserRoleForm(request.POST, instance=obj))
#     #     for e in user_role_form:
#     #         if e.is_valid():
#     #             e.save()
#     #     if form.is_valid():
#     #         form.save()
#     #         return redirect('dashboard')
#     # else:
#     #     form = ProfileUpdateForm(instance=user1)
#     #     user_role_form =[]
#     #     for obj in useroleobj:
#     #         user_role_form.append(UserRoleForm(instance=obj))
#     #         print(user_role_form)
#
#     return render(request, 'edituserprofile.html',{'uid':id})
#
# def deleteprofile(request,uid):
#     user1 = User.objects.get(pk=uid)
#     user1.delete()
#     users = UserProfile.objects.all()
#     return render(request, 'userlist.html', {'users': users})
#
#
# def userprofile(request):
#     users = UserProfile.objects.all()
#     for user in users:
#         print(user)
#     return render(request, 'userlist.html', {'users': users})
#
#
# def incidentcategoryadd(request):
#     if request.method == 'POST':
#         form = NewIncidentCategory(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('dashboard')
#     else:
#         form = NewIncidentCategory()
#
#         return render(request, 'editprofile.html', {'form': form})
#
# @staff_member_required()
# def editorganisation(request):
#     user1 = Organisation.objects.get()
#     if request.method == 'POST':
#         form = orgUpdateForm(request.POST, instance=user1)
#         if form.is_valid():
#             form.save()
#             return redirect('dashboard')
#     else:
#         form = orgUpdateForm(instance=user1)
#
#         return render(request, 'editorg.html',{'form': form})
#
#
#
# @staff_member_required()
# def CreateOrg(request):
#     org_detail = Organisation.objects.all()
#     return render(request, 'createorganisation.html', {'org_detail': org_detail})
#
# @staff_member_required()
# def CreateUser(request):
#     return render(request, 'createuser.html', {})
#
#
# def CreateIncident(request):
#     return render(request, 'createincident.html', {})



class checkuserrole(APIView):
    """
                 This function is used to restrict rom removing a role if assigned to any user
                """
    def get(self,request,pk,roleid):
        flag =0
        org =Organisation.objects.get(pk=pk)
        user = UserProfile.objects.all()
        role =Roles.objects.get(role_id=roleid)
        for euser in user:
            for rolemaps in euser.user_role_id.all():
                for eroles in rolemaps.roles.all():
                    if role == eroles:
                        flag =1
        if flag == 1:
            return JsonResponse({"status": True})
        else:
            return JsonResponse({"status": False})

class checkusercat(APIView):
    """
             This function is used to restrict rom removing a category if assigned to any user
            """
    def get(self,request,pk,catid):
        flag =0
        org =Organisation.objects.get(pk=pk)
        user = UserProfile.objects.all()
        category =IncidentCategory.objects.get(cat_id=catid)
        for euser in user:
            for cat in euser.user_category.all():
                if category == cat:
                    flag =1
        if flag == 1:
            return JsonResponse({"status": True})
        else:
            return JsonResponse({"status": False})


class Filterdata(APIView):
    """
             This function is used to filter incident data
            """
    permission_classes = (IsAuthenticated,)
    def post(self,request):
        data = request.data
        finalquerysetdata = []
        if data['category']!='':
            cat=IncidentCategory.objects.get(cat_id=data['category'])
            finalquerysetdata.append(IncidentReport.objects.filter(incident_type=cat))
        if data['fromdate'] != '':
             finalquerysetdata.append(IncidentReport.objects.filter(incident_date__gte=data['fromdate']).order_by(
                                  '-incident_date'))
        if data['todate'] != '':
            finalquerysetdata.append(IncidentReport.objects.filter(incident_date__lte=data['todate']).order_by('-incident_date'))
        if data['status'] != '':
            finalquerysetdata.append(IncidentReport.objects.filter(status=data['status']))
        if data['incidentno'] != '':
            finalquerysetdata.append(IncidentReport.objects.filter(incident_id=data['incidentno']))
        if data['reportedby'] != '':
            reporter = ReporterDetails.objects.get(email_id=data['reportedby'])
            finalquerysetdata.append(IncidentReport.objects.filter(reporter_id=reporter))

        superquery = IncidentReport.objects.all()
        for que in finalquerysetdata:
            superquery = superquery & que
        serializer_class = IncidentReportSerializer(superquery, many=True)
        return Response(serializer_class.data)


class Filteruserdata(APIView):
    """
                 This function is used to filter user data
                """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        data = request.data
        finalquerysetdata = []
        if data['role'] != '':
             role = Roles.objects.get(role_id=data['role'])
             usreswithroles = UserRole.objects.filter(roles=role)
             mydum = UserProfile.objects.none()
             for each in usreswithroles:
                user = User.objects.get(pk=each.userid.id)
                mydum |=UserProfile.objects.filter(user=user)
             finalquerysetdata.append(mydum)
        if data['email'] != '':
            user = User.objects.get(email=data['email'])
            finalquerysetdata.append(UserProfile.objects.filter(user=user))
        if data['firstname'] != '':
            finalquerysetdata.append(UserProfile.objects.filter(user_firstname=data['firstname']))
        if data['user_phone'] != '':
            finalquerysetdata.append(UserProfile.objects.filter(user_phone=data['user_phone']))
        if data['user_mobile'] != '':
            finalquerysetdata.append(UserProfile.objects.filter(user_mobile=data['user_mobile']))
        if data['category'] != '':
            finalquerysetdata.append(UserProfile.objects.filter(user_category=data['category']))
        superquery = UserProfile.objects.all()
        for que in finalquerysetdata:
             superquery = superquery & que
        serializer_class = UserProfileSerializer(superquery, many=True)
        return Response(serializer_class.data)




